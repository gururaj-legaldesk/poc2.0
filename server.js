/**
 * Created by gururaj on 4/17/17.
 */

var express       = require('express');
var path          = require('path');
var bodyParser    = require('body-parser');
var multer        = require('multer');
var fs            = require('fs');

var index         = require('./routes/index');
var users         = require('./routes/users');
var esign         = require('./routes/esign');

var mongojs       = require('mongojs');
var db            = mongojs('mongodb://localhost:27017/signdesk', ['signdesk_documents','signdesk_users','signdesk_signers']);

var port          = 4000;
var app           = express();

var clientPath = "http://localhost:4200";

//View Engine
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');
app.engine('html', require('ejs').renderFile);

// Set Static Folder
app.use(express.static(path.join(__dirname, 'client')));

app.use(function(req, res, next) { //allow cross origin requests
    res.setHeader("Access-Control-Allow-Methods", "POST, PUT, OPTIONS, DELETE, GET");
    res.header("Access-Control-Allow-Origin",clientPath);
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    res.header("Access-Control-Allow-Credentials", true);
    next();
});

// Body Parser MW
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: false}));


app.use('/', index);

app.use('/api', users);
app.use('/api/users', users);

app.use('/api/esign', esign);

/*
app.get('/!*', (req, res) => {
    res.sendFile(path.join(__dirname, 'client/dist/index.html'));
});*/



var storage = multer.diskStorage({ //multers disk storage settings
    destination: function (req, file, cb) {

        fs.stat('uploads/users/'+req.body.userId, function (err, stats){
            if (err) {         
                fs.mkdir('uploads/users/'+req.body.userId, function (err) {
                   if(!err) fs.mkdir('uploads/users/'+req.body.userId+"/signed");
                });

               return fs;
            }
        });
        cb(null, 'uploads/users/'+req.body.userId);
    },
    filename: function (req, file, cb) {
        var datetimestamp = Date.now();

        var newDocumentObj = {
            organisation_id            : req.body.organisation_id,
            department_id              : req.body.department_id,
            document_name              : req.body.document_name,
            created_by                 : req.body.created_by,
            updated_by                 : req.body.updated_by,
            document_title             : req.body.document_title,
            document_path              : req.body.document_path,
            document_source            : req.body.document_source,
            is_deleted                 : req.body.is_deleted,
            signature_sequence         : req.body.signature_sequence,
            enable_email_notification  : req.body.enable_email_notification,
            created_at                 : req.body.created_at
        }

        db.signdesk_documents.insert(newDocumentObj, function(err, documentObj){
            req.documentId   = documentObj._id;
            var uploadedName =  documentObj._id + '_document.' + file.originalname.split('.')[file.originalname.split('.').length -1]
            if(err){ console.log('Filed to Create Document'); }



            //console.log('issue',req.file.destination+"/")

            db.signdesk_documents.update({_id: mongojs.ObjectId(documentObj._id )},  {$set:{'document_path':req.body.document_path+uploadedName,'latest_document':req.body.document_path+uploadedName}}, function (err, document) {
                if(err){ console.log('Filed to update Document latest attribute');}
            });
            cb(null,uploadedName );
        });
     }
});

var upload = multer({  storage: storage }).single('file');

/** API path that will upload the files */
app.post('/upload', function(req, res) {
    upload(req,res,function(err){
        // console.log(req.file);
        if(err){
            res.json({error_code:1,err_desc:err});
            return;
        }
        res.json({error_code:0,err_desc:null,fileInfo:req.file,documentId: req.documentId});
    });
});





var excelstorage = multer.diskStorage({ //multers disk storage settings
    destination: function (req, file, cb) {
        cb(null, './uploads/import-signer/');
    },
    filename: function (req, file, cb) {
        var datetimestamp = Date.now();
        cb(null, file.fieldname + '_' + datetimestamp + '.' + file.originalname.split('.')[file.originalname.split('.').length -1]);
    }
});
var excelupload = multer({ //multer settings
    storage: excelstorage
}).single('file');
app.post('/excelUpload', function(req, res) {

    excelupload(req,res,function(err){
        // console.log(req.file);
        if(err){
            res.json({error_code:1,err_desc:err});
            return;
        }

        res.json({error_code:0,err_desc:null,fileInfo:req.file});
    });
});




/**
 * Process the request of uploading supporting file
 *
 * Callback excelupload() once processed.
 *
 * */

var supportingFilestorage = multer.diskStorage({
    destination: function (req, file, cb) {

       fs.stat('uploads/users/'+req.body.userId, function (err, stats){
            if (err) {
                fs.mkdir('uploads/users/'+req.body.userId, function (err) {
                    if(!err) fs.mkdir('uploads/users/'+req.body.userId+"/signed");
                });

                return fs;
            }
       });
       cb(null, 'uploads/users/'+req.body.userId);
    },
    filename: function (req, file, cb) {
        var datetimestamp = Date.now();
        cb(null,   'SD_'+req.body.documentId+'_' + datetimestamp + '.' + file.originalname.split('.')[file.originalname.split('.').length -1]);
    }
});
var supportingFileupload = multer({ storage: supportingFilestorage }).array('file', 10);

app.post('/supportingDocUpload', function(req, res) {

var  uploadedFiles= []
    supportingFileupload(req,res,function(err){

        if(err){ res.json({error_code:1,err_desc:err});   }


        var currentSupportingDocument={
            'id':Date.now().toString(),
            'filename':req.files[0].filename,
            'file_path':req.files[0].destination+"/",
            'deleted':false
        };

        db.signdesk_documents.findOne( {_id: mongojs.ObjectId( req.body.documentId )}  , function(err, documentInfo){
            if(err){ res.json({error_code:1,err_desc:err});   }

            var currentSupportingDocs = documentInfo.suporting_documents;


            if(!currentSupportingDocs){
                currentSupportingDocument.order = 1
                currentSupportingDocs= [currentSupportingDocument];
            }else{
                currentSupportingDocument.order = parseInt(currentSupportingDocs[currentSupportingDocs.length-1].order)+1
                currentSupportingDocs.push(currentSupportingDocument);
            }


            db.signdesk_documents.update({_id: mongojs.ObjectId(req.body.documentId )},  {$set:{'suporting_documents':currentSupportingDocs}}, function (err, document) {
                if(err){   res.json({error_code:1,err_desc:err});   }

                res.json({error_code:0,err_desc:null,fileInfo:req.files});
            });

        });



    });
});





/*app.use(logErrors)
app.use(clientErrorHandler)
app.use(errorHandler)*/

app.listen(port, function(){
    console.log('Server started on port '+port);
});
/*

function logErrors (err, req, res, next) {
    console.error(err.stack)
    next(err)
}


function clientErrorHandler (err, req, res, next) {
    if (req.xhr) {
        res.status(500).send({ error: 'Something failed!' })
    } else {
        next(err)
    }
}

function errorHandler (err, req, res, next) {
    res.status(500)
    res.render('error', { error: err })
}*/