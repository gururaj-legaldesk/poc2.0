import { NgModule }            from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { WorkflowComponent }      from './workflow.component';
import { WorkflowRoutingModule }  from './workflow-routing.module';
import { WorkflowService } from '../services/workflow.service';

@NgModule({
  imports: [
    WorkflowRoutingModule,CommonModule,FormsModule
  ],
  declarations: [ WorkflowComponent ],
  providers: [ 
    WorkflowService
  ]
})
export class WorkflowModule { }
