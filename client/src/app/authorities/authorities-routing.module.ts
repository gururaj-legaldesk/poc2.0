import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AuthoritiesComponent } from './authorities.component';

const routes: Routes = [
  {
    path: '',
    component: AuthoritiesComponent,
    data: {
      title: 'authorities'
    }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AuthoritiesRoutingModule {}
