import { NgModule }            from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { AuthoritiesComponent }      from './authorities.component';
import { AuthoritiesRoutingModule }  from './authorities-routing.module';
import { TemplateService } from '../services/template.service';

@NgModule({
  imports: [
    AuthoritiesRoutingModule,CommonModule,FormsModule
  ],
  declarations: [ AuthoritiesComponent ],
  providers: [ 
    TemplateService
  ]
})
export class AuthoritiesModule { }
