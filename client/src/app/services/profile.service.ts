import {Injectable} from '@angular/core';
import {Http, Headers , Response} from '@angular/http';

import 'rxjs/add/operator/map';
import { Config } from '../config';

@Injectable()
export class ProfileService{
    public appConfig  = new Config();

    constructor(private http:Http){
        console.log('Task Service Initialized...');
    }

    getProfile(userId){

        return this.http.get(this.appConfig.serverAddress+'/api/esign/workflow/'+userId)
            .map(res => res.json());
    }

}
