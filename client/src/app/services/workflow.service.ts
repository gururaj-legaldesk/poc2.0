import {Injectable} from '@angular/core';
import {Http, Headers , Response} from '@angular/http';

import 'rxjs/add/operator/map';
import { Config } from '../config';


@Injectable()
export class WorkflowService{

    public appConfig           = new Config();

    constructor(private http:Http){
        console.log('Task Service Initialized...');
    }

    getWorkflows(userId){

        return this.http.get(this.appConfig.serverAddress+'/api/esign/workflow/'+userId)
            .map(res => res.json());
    }

}
