import { NgModule }            from '@angular/core';
import { CommonModule }        from '@angular/common';
import { FormsModule } from '@angular/forms';

import { EsignComponent }      from './esign.component';
import { EsignRoutingModule }  from './esign-routing.module';
//import { FileSelectDirective, FileDropDirective } from 'ng2-file-upload';
import { EsignService } from '../services/esign.service';

@NgModule({
  imports: [
    EsignRoutingModule,CommonModule,FormsModule
  ],
  declarations: [ EsignComponent ],
  providers: [ //
    EsignService
  ]
})
export class EsignModule { }
