import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { EsignComponent } from './esign.component';

const routes: Routes = [
  {
    path: '',
    component: EsignComponent,
    data: {
      title: 'eSign'
    }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class EsignRoutingModule {}
