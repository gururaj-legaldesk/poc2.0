import { Component, OnInit } from '@angular/core';
import { Router,ActivatedRoute } from '@angular/router';
import {Http, Headers} from '@angular/http';
import 'rxjs/add/operator/map';
import { DocumentService } from '../services/document.service';
import { EsignService } from '../services/esign.service';
import { Config } from '../config';

@Component({
  templateUrl: 'viewsupportingdocument.component.html'
})

export class ViewSupportingDocumentComponent implements OnInit {


  public appConfig           = new Config();

  public sourceType:string =  '';
  public currentStep:string =  'step1';


  public documentId:string

  public documentTitle:string
  public filename:string
  public fileSource:string
  public createdDateTime:string

  public signerList          = []
  // Set our default values
  localState = { value: '' };
  fileName ='';
  askAadhaar:boolean;
  askAadhaarOTP =false;
  aadhaar:string;
  aadhaarOTP:string;
  aadhaarError :boolean;
  aadhaarOtpError :boolean;
  eSignResponse: boolean;
  termsandconditions :boolean;
  esignedFileSource :string;
  uploadedFileName:string;
  transactionId:string;
  loading:boolean; otpError:boolean;
 documentsList: Array<number> = [];
 supportDocId:string;


  // TypeScript public modifiers
  constructor( private router: Router ,private http:Http,private esignService:EsignService,private route:ActivatedRoute) {
    this.sourceType  = "";

    this.aadhaar=  "";
  }


  fileEvent(fileInput: any){
    let file = fileInput.target.files[0];
    this.fileName = file.name;


    if (fileInput.target.files && fileInput.target.files[0]) {
      var reader = new FileReader();

      reader.onload = function (e : any) {
        (<HTMLInputElement>document.getElementById('iframe')).src= e.target.result;

        // document.getElementById('iframe').src= e.target.result;
        //   console.log('file was selected:' , e.target.result);
        //  $('#preview').attr('src', e.target.result);
      }

      reader.readAsDataURL(fileInput.target.files[0]);


    }
  }



 


  downloadFile() {
    //  this.esignService.downloadEsignedFile(this.fileName )
  }

  



  ngOnInit(): void {
    this.route.params.subscribe(params => {
      this.documentId=params['documentId']
       this.supportDocId=params['supportingDocId']
 
 this.fileSource     = this.appConfig.serverAddress+'/api/esign/viewsupportingfile/'+this.documentId+'/'+this.supportDocId;
        console.log(this.fileSource);
          (<HTMLInputElement>document.getElementById('file_preview')).src= this.fileSource;


    });
  }
}
