import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Http, Headers } from '@angular/http';
import { DomSanitizer } from '@angular/platform-browser';



import 'rxjs/add/operator/map';
import { DocumentService } from '../services/document.service';
import { EsignService } from '../services/esign.service';
import { FileUploader } from 'ng2-file-upload';
import { Document } from '../schema/document';
import { Signer } from '../schema/signer';


import { Config } from '../config';

import { ToasterModule, ToasterService, ToasterConfig } from 'angular2-toaster/angular2-toaster';

@Component({
  templateUrl: 'createdocument.component.html'
})

export class CreateDocumentComponent implements OnInit {

  public sourceType: string = '';
  public currentStep: string = 'step1';
  public newDocumentObj = new Document();
  public newSignerObj = new Signer();
  public appConfig = new Config();

  public signer_name: string = '';
  public signer_email: string = '';
  public signer_mobile: number;
  public noOfSigners: string;
  public isFirstSigner: string

  public userId: string = '592fd304f066a64c22ddab21';
  public signerList = [];
  public signerId: string
  public validEmailId: boolean = true

  public uploadedFilePath: any = '';
  public addSupportingDoc: boolean = false;

  public iframeSafeResourceUrl: any;

  esignError: boolean = false;
  esignFailed: boolean = false;
  esignErrorCode: string;
  esignErrorDesc: string;


  signer_aadhaar: string;
  signer_otp: string;

  // Set our default values
  importOption = false
  localState = { value: '' };
  fileName = '';
  askAadhaar: boolean;
  askAadhaarOTP = false;


  aadhaarError: boolean;
  aadhaarOtpError: boolean;
  eSignResponse: boolean;
  termsandconditions: boolean;
  esignedFileSource: string;
  uploadedFileName: string;
  transactionId: string;
  loading: boolean;
  otpError: boolean = false;
  documentId: string;
  self_esign: boolean = false;

  private toasterService: ToasterService;

  public toasterconfig: ToasterConfig =
  new ToasterConfig({
    tapToDismiss: true,
    timeout: 5000
  });

  public uploader: FileUploader = new FileUploader({ url: this.appConfig.serverAddress + '/upload' }); //,autoUpload: true

  public excelUploader: FileUploader = new FileUploader({ url: this.appConfig.serverAddress + '/excelUpload' });

  public supportingDocUploader: FileUploader = new FileUploader({ url: this.appConfig.serverAddress + '/supportingDocUpload' });


  // public excelUploader:FileUploader = new FileUploader({url:this.appConfig.serverAddress+'/api/esign/excelUpload'});

  // TypeScript public modifiers
  constructor(private router: Router, private http: Http, private esignService: EsignService, toasterService: ToasterService, private sanitizer: DomSanitizer) {
    this.sourceType = "";
    this.currentStep = 'step1';
    this.signer_aadhaar = "";
    this.toasterService = toasterService;
    // Add in the other upload form parameters.

    this.uploader.onBuildItemForm = (item, form) => {
      form.append('userId', this.userId);
    };




  }




  fileEvent(fileInput: any) {
    let file = fileInput.target.files[0];
    this.fileName = file.name;

    if (fileInput.target.files && fileInput.target.files[0]) {
      var reader = new FileReader();
      console.log(this.uploader);

      reader.onload = function (e: any) {
        (<HTMLIFrameElement>document.getElementById('file_preview')).src = e.target.result;
      }
      reader.readAsDataURL(fileInput.target.files[0]);
    }
  }

  getSignedDocument() {
    if (!this.signer_aadhaar) {
      this.aadhaarError = true
    } else if (this.signer_aadhaar && this.signer_aadhaar.toString().length < 4) {
      this.aadhaarError = true
    } else {
      this.aadhaarError = false
      this.loading = true

      this.esignService.getTriggarOTP(this.signer_aadhaar)
        .subscribe(response => {
          try {
            var responseJson = JSON.parse(response);
            this.loading = false

            if (responseJson.ErrorCode == "NA") {
              this.currentStep = 'step5';
              this.transactionId = responseJson.Transaction_Id;
              this.otpError = false
            } else {
              this.currentStep = 'step4';
              this.otpError = true
              this.loading = false
            }
          } catch (e) {
            this.currentStep = 'step4';
            this.otpError = true
            this.loading = false
          }
        },
        err => {
          this.currentStep = 'step4';
          this.otpError = true
          this.loading = false
        }

        );
    }
  }


  retryEsign() {
    console.log("retry");
    this.otpError = false
    this.loading = false
    this.currentStep = 'step4';
    this.esignFailed = false;
  }

  validateAadhaar() {
    if (!this.signer_aadhaar) {
      this.aadhaarError = true
    } else if (this.signer_aadhaar && this.signer_aadhaar.toString().length < 12) {
      this.aadhaarError = true
    } else {
      this.aadhaarError = false
      this.loading = true


      this.esignService.getTriggarOTP(this.signer_aadhaar)
        .subscribe(response => {
          try {
            var responseJson = JSON.parse(response);
            this.loading = false

            if (responseJson.ErrorCode == "NA") {
              console.log(responseJson.ESP)

              var param = {
                documentId: this.documentId,
                signerId: this.signerId,
                ESP: responseJson.ESP
              }

              this.esignService.saveEspUsed(param)
                .subscribe(response => {
                });


              this.currentStep = 'step5';
              this.transactionId = responseJson.Transaction_Id;
              this.otpError = false
              this.esignError = false;
            } else {
              this.currentStep = 'step4';
              this.otpError = true
              this.loading = false
              this.handleESignError(responseJson.ErrorCode)
            }
          } catch (e) {
            this.currentStep = 'step4';
            this.otpError = true
            this.loading = false

          }
        },
        err => {
          this.currentStep = 'step4';
          this.otpError = true
          this.loading = false
        }

        );


    }
  }


  validateOTP() {

    if (!this.signer_otp) {
      this.aadhaarOtpError = true
    } else if (this.signer_otp && this.signer_otp.toString().length < 6) {
      this.aadhaarOtpError = true
    } else if (!this.termsandconditions) {
      this.aadhaarOtpError = true
    } else {
      this.aadhaarOtpError = false
      this.esignError = false;
      //invoke Esign api here
      this.loading = true
      this.esignService.getEspUsed(this.signerId)
        .subscribe(response => {
          var esp = response;
          console.log("esp:" + response)
          var params = {
            aadhaar: this.signer_aadhaar,
            otp: this.signer_otp,
            transactionId: this.transactionId, //"c5292174c75739ba7b8fb9898a0c854c",, //this.transactionId,
            sourcefile: "uploads/users/" + this.userId + "/" + this.uploadedFileName,
            referenceId: "signdesk" + Date.now().toString(), //this.transactionId
            outputFile: "uploads/users/" + this.userId + "/signed/" + this.documentId + "_document_v1_0.pdf",
            signerSequence: (this.signerList.length),
            noOfSigners: this.signerList.length,
            isFirstSigner: "true",
            documentId: this.documentId,
            userId: this.userId,
            esp: esp
          }

          this.esignService.getSignedDocument(params)
            .subscribe(response => {
              console.log(response)

              this.loading = false
              this.askAadhaarOTP = false


              if (response.toString().trim() == "NA") {

                this.currentStep = 'step6';
                this.otpError = false;
                this.esignError = false;

                this.esignedFileSource = this.appConfig.serverAddress + '/api/esign/download/' + this.documentId;

                this.uploadedFilePath = this.sanitizer.bypassSecurityTrustResourceUrl(this.appConfig.serverAddress + '/api/esign/viewfile/' + this.documentId);
                (<HTMLIFrameElement>document.getElementById('file_preview')).src = this.uploadedFilePath;


                var param = {
                  documentId: this.documentId,
                  signerId: this.signerId,
                  status: 'signed'
                }

                this.esignService.updateSignerStatus(param)
                  .subscribe(response => {
                  });


                this.esignService.notifySignerByOrder({
                  documentId: this.documentId
                })
                  .subscribe(response => {
                  });





              } else if (response.toString().trim() == "es_114") {
                console.log(response)
                this.esignFailed = true;
                this.currentStep = 'step7';
                this.askAadhaarOTP = false
                this.otpError = true
                this.loading = false
                this.esignError = true;
                this.signer_otp = "";
                this.handleESignError(response.toString().trim())
              } else {
                this.currentStep = 'step5';
                this.askAadhaarOTP = false
                this.otpError = true
                this.loading = false

                this.esignError = true;
                this.handleESignError(response.toString().trim())

              }
            });
        });
    }
  }


  saveDocument(event) {
    event.preventDefault();

    if (this.supportingDocUploader.getNotUploadedItems().length > 0) {

      this.supportingDocUploader.onBuildItemForm = (item, form) => {
        form.append('documentId', this.documentId);
        form.append('userId', this.userId);
      };

      console.log('upload files ', this.supportingDocUploader.getNotUploadedItems().length)

      this.supportingDocUploader.uploadAll()

      this.supportingDocUploader.onCompleteItem = (item: any, response: any, status: any, headers: any) => {
        var responsePath = JSON.parse(response);

        console.log('after upload', response)

        if (this.self_esign) {
          this.newSignerObj.document_id = this.documentId
          this.newSignerObj.signer_index = (this.signerList.length + 1)
          this.newSignerObj.external_user = false
          this.newSignerObj.user_id = ''
          this.newSignerObj.signature_sequence = (this.signerList.length + 1)
          this.newSignerObj.email_notification = true
          this.newSignerObj.signature_page = 'all' //
          this.newSignerObj.signature_position = {
            page: 'all',
            position: 'bottom-left'
          }
          this.newSignerObj.status = "invited"
          this.newSignerObj.invited_at = new Date()
          this.newSignerObj.invited_by = this.userId
          this.newSignerObj.remainders = {}

          var signerData = {
            signer_name: 'Demo User',
            signer_email: 'gururaj.shetty@legaldesk.com',
            signer_mobile: '9164977172',
            signerInfo: this.newSignerObj
          }

          this.esignService.addSigner(signerData)
            .subscribe(response => {
              this.signerId = response._id;
              this.signerList.push(response)
              this.currentStep = 'step4';
            });


        } else {
          this.currentStep = 'step3';
          var params = { documentId: this.documentId }
          this.esignService.notifySignerByOrder(params)
            .subscribe(response => { });
        }
      };

    } else {

      if (this.self_esign) {

        this.newSignerObj.document_id = this.documentId
        this.newSignerObj.signer_index = (this.signerList.length + 1)
        this.newSignerObj.external_user = false
        this.newSignerObj.user_id = ''
        this.newSignerObj.signature_sequence = (this.signerList.length + 1)
        this.newSignerObj.email_notification = true
        this.newSignerObj.signature_page = 'all' //
        this.newSignerObj.signature_position = {
          page: 'all',
          position: 'bottom-left'
        }

        this.newSignerObj.status = "invited"
        this.newSignerObj.invited_at = new Date()
        this.newSignerObj.invited_by = this.userId
        this.newSignerObj.remainders = {}

        var signerData = {
          signer_name: 'Demo User',
          signer_email: 'gururaj.shetty@legaldesk.com',
          signer_mobile: '9164977172',
          signerInfo: this.newSignerObj
        }

        this.esignService.addSigner(signerData)
          .subscribe(response => {
            this.signerId = response._id;

            this.signerList.push(response)
            this.currentStep = 'step4';
          });


      } else {

        this.currentStep = 'step3';
        var params = { documentId: this.documentId }
        this.esignService.notifySignerByOrder(params)
          .subscribe(response => { });

      }
    }
  }

  newDocument() {
    this.askAadhaar = true
    this.askAadhaarOTP = false
    this.eSignResponse = false
    this.fileName = ''
    this.signer_otp = ''
    this.signer_aadhaar = ''
    this.termsandconditions = false
  }

  downloadFile() {
    //  this.esignService.downloadEsignedFile(this.fileName )
  }

  removeFile(item){
    item.remove();
  }


  uploadFile(item) {
    var resp;

    this.uploader.onBuildItemForm = (item, form) => {
      form.append('userId', this.userId);
      form.append('organisation_id', 'Deloitte_12112');
      form.append('department_id', 'Deloitte_Dep_32232');
      form.append('document_name', item._file.name);
      form.append('created_by', this.userId);
      form.append('updated_by', this.userId);
      form.append('document_title', 'Document - ' + item._file.name);
      form.append('document_path', "uploads/users/" + this.userId + "/");
      form.append('document_source', 'local');
      form.append('is_deleted', false);
      form.append('signature_sequence', 'sequence');
      form.append('enable_email_notification', false);
      form.append('created_at', new Date());
    };

    resp = item.upload();

    this.uploader.onCompleteItem = (item: any, response: any, status: any, headers: any) => {

      var responsePath = JSON.parse(response);
      this.askAadhaar = true;
      this.uploadedFilePath = this.sanitizer.bypassSecurityTrustResourceUrl(this.appConfig.serverAddress + '/api/esign/viewfile/' + responsePath.documentId);

      this.uploadedFileName = responsePath.fileInfo.filename
      this.documentId = responsePath.documentId
      this.currentStep = 'step2';
      //(<HTMLIFrameElement>document.getElementById('file_preview')).src= this.sanitizer.bypassSecurityTrustResourceUrl(this.uploadedFilePath);


      console.log('uploaded file name ', responsePath.fileInfo.filename);
      console.log('created document id ', this.documentId);
      console.log('uploadedFilePath ', this.uploadedFilePath);


    };

  }



  importFile(item) {
    var resp;
    resp = item.upload();

    this.excelUploader.onCompleteItem = (item: any, response: any, status: any, headers: any) => {
      var responsePath = JSON.parse(response);



      this.esignService.getNoOfSigners(this.documentId)
        .subscribe(response => {
          console.log(response.length);
          if (response.length <= 0) {
            var signerIndex = 1;
          } else {
            var signerIndex = parseInt(response.length + 1);
          }

          var importInfo = {
            filename: responsePath.fileInfo.filename,
            documentId: this.documentId,
            signerIndex: signerIndex
          }


          this.esignService.importSigner(importInfo)
            .subscribe(response => {

              for (var i = 0; i < response.length; i++) {
                this.signerList.push(response[i]);
              }

              this.importOption = false;
              item.cancel()
              this.excelUploader.clearQueue()
              this.toasterService.pop('success', 'Successful', 'Imported Successfully.');
            });

        });
    };


  }

  addSigner(event) {
    event.preventDefault();


    var pattern = /^[\w._-]+[+]?[\w._-]+@[\w.-]+\.[a-zA-Z]{2,6}$/;

    this.validEmailId =pattern.test(this.signer_email)

     if(this.validEmailId){
        this.newSignerObj.document_id = this.documentId
        this.newSignerObj.signer_index = (this.signerList.length + 1)
        this.newSignerObj.external_user = false
        this.newSignerObj.user_id = ''
        this.newSignerObj.signature_sequence = (this.signerList.length + 1)
        this.newSignerObj.email_notification = false
        this.newSignerObj.signature_page = 'all' //
        this.newSignerObj.signature_position = {
        page: 'all',
        position: 'bottom-left'
        }
        this.newSignerObj.status = "invited"
        this.newSignerObj.invited_at = new Date()
        this.newSignerObj.invited_by = this.userId
        this.newSignerObj.remainders = {}

        var signerData = {
        signer_name: this.signer_name,
        signer_email: this.signer_email,
        signer_mobile: this.signer_mobile,
        signerInfo: this.newSignerObj
        }


        this.esignService.addSigner(signerData)
        .subscribe(response => {
        this.signerList.push(response)
        this.signer_name = '';
        this.signer_email = '';
        this.signer_mobile = null
        console.log(response)
        });

     }



  }


  deleteSigner(signer) {
    var signerList = this.signerList;

    this.esignService.deleteSigner(signer._id).subscribe(data => {

      this.toasterService.pop('success', 'Successful', 'Signer Removed Successfully.');
      console.log('data', data)
      if (data.n == 1) {
        for (var i = 0; i < signerList.length; i++) {
          if (signerList[i]._id == signer._id) {
            signerList.splice(i, 1);
          }
        }
      }
    });
  }

  /*
    this.esignService.sendEmail({})
  .subscribe(response => {
    console.log(response)
  });
  */




  ngOnInit(): void {
    //generate random values for mainChart

    //this.currentStep = 'step5';



  }

  handleESignError(errorCode) {
    this.esignErrorCode = errorCode

    switch (errorCode) {
      case 'es_100':
        this.esignErrorDesc = "Invalid File Format"
        break;
      case 'es_101':
        this.esignErrorDesc = "Aadhar Number entered is incorrect"
        break;
      case 'es_102':
        this.esignErrorDesc = "There is no mobile number registered with this Aadhaar Number"
        break;
      case 'es_103':
        this.esignErrorDesc = "OTP you have entered is incorrect"
        break;
      case 'es_104':
        this.esignErrorDesc = "Invalid Aadhaar Number Or Non Availability of Aadhaar data"
        break;
      case 'es_106':
        this.esignErrorDesc = "OTP must contain 6 digits"
        break;
      case 'es_107':
        this.esignErrorDesc = "Invalid Signature Position"
        break;
      case 'es_108':
        this.esignErrorDesc = "Invalid Document_Id"
        break;
      case 'es_109':
        this.esignErrorDesc = "Invalid Consent Input"
        break;
      case 'es_110':
        this.esignErrorDesc = "An error occurred while signing"
        break;
      case 'es_111':
        this.esignErrorDesc = "Sorry, seems there is an issue. Contact SignDesk.com"
        break;
      case 'es_112':
        this.signer_otp = "";
        this.esignErrorDesc = "OTP you have entered is incorrect"
        break;
      case 'es_114':
        this.esignErrorDesc = "Sorry, seems there is an issue."
        break;
      case 'es_115':
        this.esignErrorDesc = "Invalid X-Parse-Rest-Api-Key"
        break;
      case 'es_116':
        this.esignErrorDesc = "Invalid X-Parse-Application-Id"
        break;
      case 'es_117':
        this.esignErrorDesc = "X-Parse-Application-Id Not Found in Header"
        break;
      case 'es_118':
        this.esignErrorDesc = "X-Parse-Rest-Api-Key Not Found in Header"
        break;
      case 'es_119':
        this.esignErrorDesc = "Character length is more than 30"
        break;
      case 'es_120':
        this.esignErrorDesc = 'Json attribute <name of attribute> Not Found in Request'
        break;

      case 'es_121':
        this.esignErrorDesc = 'This ReferenceID <ReferenceID number> is already used(It occurs when same ReferenceID is used for more than one file)'
        break;

      case 'es_122':
        this.esignErrorDesc = "Invalid TimeStamp Value"
        break;

      case 'es_123':
        this.esignErrorDesc = "X-Parse-Application-Id and X-Parse-Rest-Api-Key Not Matching"
        break;

      case 'es_124':
        this.esignErrorDesc = "Invalid Email Address"
        break;
      case 'es_125':
        this.esignErrorDesc = "Not Valid Subscriber"
        break;

      case 'es_601':
        this.esignErrorDesc = "Duplicate Reference Id"
        break;

      case 'es_602':
        this.esignErrorDesc = "Invalid PayLoad or PayLoad_Hash"
        break;

      case 'es_603':
        this.esignErrorDesc = "PayLoad or PayLoad_Hash attribute not found in Request"
        break;

      case 'es_308':
        this.esignErrorDesc = "Invalid consent text"
        break;

      case 'es_301':
        this.esignErrorDesc = "Invalid Transaction Id"
        break;

      case 'es_302':
        this.esignErrorDesc = "Invalid Device Name"
        break;
      case 'es_303':
        this.esignErrorDesc = "Invalid Device Serial Number"
        break;
      case 'es_304':
        this.esignErrorDesc = "Duplicate Reference Id"
        break;
      default:
        this.esignErrorDesc = "Unknown Error"

    }
  }

}
