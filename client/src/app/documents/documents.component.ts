import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import {Http, Headers} from '@angular/http';
import 'rxjs/add/operator/map';
import { DocumentService } from '../services/document.service';

import { Config } from '../config';

@Component({
  templateUrl: 'documents.component.html'
})
export class DocumentsComponent implements OnInit {

  public brandPrimary:string =  '#20a8d8';
  public brandSuccess:string =  '#4dbd74';
  public brandInfo:string    =   '#67c2ef';
  public brandWarning:string =  '#f8cb00';
  public brandDanger:string  =   '#f86c6b';

  public appConfig           = new Config();

  // Set our default values
  localState = { value: '' };
  loading:boolean;
  userId:string;

  documentsList: Array<number> = [];

  createRange(number){
    var items: number[] = [];
    for(var i = 1; i <= number; i++){
      items.push(i);
    }
    return items;
  }

  // TypeScript public modifiers
  constructor( private router: Router ,private http:Http,private documentService:DocumentService) {
    this.userId=  "";
  }

  ngOnInit(): void {

    console.log('sdsds');
    this.documentService.getDocuments()
      .subscribe(response => {
        this.documentsList=response;
console.log(this.documentsList)
      });
  }
}
