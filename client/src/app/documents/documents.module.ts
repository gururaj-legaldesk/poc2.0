import { NgModule }            from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
// Notifications
import { ToasterModule, ToasterService} from 'angular2-toaster/angular2-toaster';

import { DocumentsRoutingModule }  from './documents-routing.module';

import { DocumentService }         from '../services/document.service';
import { EsignService }            from '../services/esign.service';

import { DocumentsComponent }      from './documents.component';
import { CreateDocumentComponent } from './createdocument.component';
import { ViewDocumentComponent } from './viewdocument.component';
import { ViewSupportingDocumentComponent } from './viewsupportingdocument.component';

import { SignDocumentComponent } from './signdocument.component';

// Modal Component
import { ModalModule } from 'ngx-bootstrap/modal';
import { ModalsComponent } from '../components/modals.component';

import { FileSelectDirective, FileDropDirective } from 'ng2-file-upload';


@NgModule({
  imports: [
    DocumentsRoutingModule,CommonModule,FormsModule,ToasterModule, ModalModule.forRoot()
  ],
  declarations: [ DocumentsComponent,CreateDocumentComponent,FileSelectDirective,ViewSupportingDocumentComponent,ViewDocumentComponent,SignDocumentComponent,ModalsComponent],
  providers: [
    DocumentService,
    EsignService
  ]
})
export class DocumentsModule {

}
