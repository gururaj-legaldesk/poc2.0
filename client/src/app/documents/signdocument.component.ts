import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Http, Headers } from '@angular/http';
import 'rxjs/add/operator/map';
import { DocumentService } from '../services/document.service';
import { EsignService } from '../services/esign.service';
import { FileUploader } from 'ng2-file-upload';
import { Config } from '../config';

@Component({
  templateUrl: 'signdocument.component.html'
})

export class SignDocumentComponent implements OnInit {

  public appConfig = new Config();
  public sourceType: string = '';
  public currentStep: string = 'step1';

  public documentId: string
  public signerId: string
  public signerSequence: string
  public noOfSigners: string

  public isFirstSigner: boolean
  public documentTitle: string
  public filename: string
  public fileSource: string
  public createdDateTime: string
  public userId: string

  esignFailed: boolean = false;

  signer_aadhaar: string;
  signer_otp: string;

  latest_document: string;
  document_path: string;


  // Set our default values
  localState = { value: '' };
  fileName = '';
  askAadhaar: boolean;
  askAadhaarOTP = false;
  aadhaar: string;
  aadhaarOTP: string;
  aadhaarError: boolean;
  aadhaarOtpError: boolean;
  eSignResponse: boolean;
  termsandconditions: boolean;
  esignedFileSource: string;
  uploadedFileName: string;
  transactionId: string;
  loading: boolean; otpError: boolean;

  esignError: boolean = false
  esignErrorCode: string;
  esignErrorDesc: string;

  documentsList: Array<number> = [];


  public uploader: FileUploader = new FileUploader({ url: this.appConfig.serverAddress + '/upload' });

  // TypeScript public modifiers
  constructor(private router: Router, private http: Http, private esignService: EsignService, private route: ActivatedRoute) {
    this.sourceType = "";
    this.currentStep = 'step4';
    this.aadhaar = "";
  }

  fileEvent(fileInput: any) {
    let file = fileInput.target.files[0];
    this.fileName = file.name;

    if (fileInput.target.files && fileInput.target.files[0]) {
      var reader = new FileReader();

      reader.onload = function (e: any) {
        (<HTMLInputElement>document.getElementById('iframe')).src = e.target.result;

        // document.getElementById('iframe').src= e.target.result;
        //   console.log('file was selected:' , e.target.result);
        //  $('#preview').attr('src', e.target.result);
      }
      reader.readAsDataURL(fileInput.target.files[0]);
    }
  }


  validateAadhaar() {
    if (!this.signer_aadhaar) {
      this.aadhaarError = true
    } else if (this.signer_aadhaar && this.signer_aadhaar.toString().length < 12) {
      this.aadhaarError = true
    } else {
      this.aadhaarError = false
      this.loading = true


      this.esignService.getTriggarOTP(this.signer_aadhaar)
        .subscribe(response => {
          try {
            var responseJson = JSON.parse(response);
            this.loading = false
            if (responseJson.ErrorCode == "NA") {

              console.log(responseJson.ESP)
              var param = {
                documentId: this.documentId,
                signerId: this.signerId,
                ESP: responseJson.ESP
              }

              this.esignService.saveEspUsed(param)
                .subscribe(response => {
                });

              this.currentStep = 'step5';
              this.transactionId = responseJson.Transaction_Id;
              this.otpError = false;
              this.esignError = false;
            } else {
              this.currentStep = 'step4';
              this.otpError = true
              this.loading = false
              // this.esignError    = true;
              this.handleESignError(responseJson.ErrorCode)
            }
          } catch (e) {
            this.currentStep = 'step4';
            this.otpError = true
            this.loading = false
          }
        },
        err => {
          this.currentStep = 'step4';
          this.otpError = true
          this.loading = false
        }

        );

    }
  }

  retryEsign() {
    console.log("retry");
    this.otpError = false
    this.loading = false
    this.currentStep = 'step4';
    this.esignFailed = false;
  }

  validateOTP() {


    if (!this.signer_otp) {
      this.aadhaarOtpError = true
    } else if (this.signer_otp && this.signer_otp.toString().length < 6) {
      this.aadhaarOtpError = true
    } else if (!this.termsandconditions) {
      this.aadhaarOtpError = true
    } else {
      this.aadhaarOtpError = false

      //invoke Esign api here
      this.loading = true




      this.esignedFileSource = this.appConfig.serverAddress + '/api/esign/download/' + this.documentId;


      this.esignError = false;

      this.esignService.getNoOfSigners(this.documentId)
        .subscribe(response => {
          this.noOfSigners = response.length.toString();
          console.log(response);

          this.esignService.getEspUsed(this.signerId)
            .subscribe(response => {
              var esp = response;
              console.log("esp:" + response)




              var params = {
                aadhaar: this.signer_aadhaar,
                otp: this.signer_otp,
                transactionId: this.transactionId, //"c5292174c75739ba7b8fb9898a0c854c",, //this.transactionId,
                sourcefile: this.latest_document,
                referenceId: "signdesk" + Date.now().toString(), //this.transactionId
                outputFile: "uploads/users/" + this.userId + "/signed/" + this.documentId + "_document_v" + this.signerSequence + "_0.pdf",
                signerSequence: this.signerSequence,//"2",
                noOfSigners: this.noOfSigners,
                isFirstSigner: this.isFirstSigner,
                documentId: this.documentId,
                userId: this.userId,
                esp: esp
              }

              this.esignService.getSignedDocument(params)
                .subscribe(response => {
                  console.log(response)

                  this.loading = false
                  this.askAadhaarOTP = false

  

                  if (response.toString().trim() == "NA") {
                    this.currentStep = 'step6';
                    this.otpError = false;
                    this.esignError = false;

                    this.fileSource = this.appConfig.serverAddress + '/api/esign/viewfile/' + this.documentId;
                    (<HTMLInputElement>document.getElementById('file_preview')).src = this.fileSource



                    var param = {
                      documentId: this.documentId,
                      signerId: this.signerId,
                      status: 'signed'
                    }

                    this.esignService.updateSignerStatus(param)
                      .subscribe(response => {
                      });



                    var params = {
                      documentId: this.documentId
                    }

                    this.esignService.notifySignerByOrder(params)
                      .subscribe(response => {
                      });



                  } else if (response.toString().trim() == "es_114") {
                    console.log(response)
                    this.esignFailed = true;
                    this.currentStep = 'step7';
                    this.askAadhaarOTP = false
                    this.otpError = true
                    this.loading = false
                    this.esignError = true;
                    this.signer_otp = "";
                    this.handleESignError(response.toString().trim())
                  } else {
                    this.currentStep = 'step5';
                    this.askAadhaarOTP = false
                    this.otpError = true
                    this.loading = false

                    this.esignError = true;
                    this.handleESignError(response.toString().trim())

                  }
                }, err => {
                  this.currentStep = 'step5';
                  this.askAadhaarOTP = false
                  this.otpError = true
                  this.loading = false
                  this.esignError = true;
                });





            });
        });


    }
  }


  handleESignError(errorCode) {
    this.esignErrorCode = errorCode
    console.log(errorCode)
    switch (errorCode) {
      case 'es_100':
        this.esignErrorDesc = "Invalid File Format"
        break;
      case 'es_101':
        this.esignErrorDesc = "Aadhar Number entered is incorrect"
        break;
      case 'es_102':
        this.esignErrorDesc = "There is no mobile number registered with this Aadhaar Number"
        break;
      case 'es_103':
        this.esignErrorDesc = "OTP you have entered is incorrect"
        break;
      case 'es_104':
        this.esignErrorDesc = "Invalid Aadhaar Number Or Non Availability of Aadhaar data"
        break;
      case 'es_106':
        this.esignErrorDesc = "OTP must contain 6 digits"
        break;
      case 'es_107':
        this.esignErrorDesc = "Invalid Signature Position"
        break;
      case 'es_108':
        this.esignErrorDesc = "Invalid Document_Id"
        break;
      case 'es_109':
        this.esignErrorDesc = "Invalid Consent Input"
        break;
      case 'es_110':
        this.esignErrorDesc = "An error occurred while signing"
        break;
      case 'es_111':
        this.esignErrorDesc = "Sorry, seems there is an issue. Contact SignDesk.com"
        break;
      case 'es_112':
        this.signer_otp = "";
        this.esignErrorDesc = "OTP you have entered is incorrect"
        break;
      case 'es_114':
        this.esignFailed = true;
        this.currentStep = 'step7';
        this.esignErrorDesc = "Sorry, seems there is an issue."
        break;
      case 'es_115':
        this.esignErrorDesc = "Invalid X-Parse-Rest-Api-Key"
        break;
      case 'es_116':
        this.esignErrorDesc = "Invalid X-Parse-Application-Id"
        break;
      case 'es_117':
        this.esignErrorDesc = "X-Parse-Application-Id Not Found in Header"
        break;
      case 'es_118':
        this.esignErrorDesc = "X-Parse-Rest-Api-Key Not Found in Header"
        break;
      case 'es_119':
        this.esignErrorDesc = "Character length is more than 30"
        break;
      case 'es_120':
        this.esignErrorDesc = 'Json attribute <name of attribute> Not Found in Request'
        break;

      case 'es_121':
        this.esignErrorDesc = 'This ReferenceID <ReferenceID number> is already used(It occurs when same ReferenceID is used for more than one file)'
        break;

      case 'es_122':
        this.esignErrorDesc = "Invalid TimeStamp Value"
        break;

      case 'es_123':
        this.esignErrorDesc = "X-Parse-Application-Id and X-Parse-Rest-Api-Key Not Matching"
        break;

      case 'es_124':
        this.esignErrorDesc = "Invalid Email Address"
        break;
      case 'es_125':
        this.esignErrorDesc = "Not Valid Subscriber"
        break;

      case 'es_601':
        this.esignErrorDesc = "Duplicate Reference Id"
        break;

      case 'es_602':
        this.esignErrorDesc = "Invalid PayLoad or PayLoad_Hash"
        break;

      case 'es_603':
        this.esignErrorDesc = "PayLoad or PayLoad_Hash attribute not found in Request"
        break;

      case 'es_308':
        this.esignErrorDesc = "Invalid consent text"
        break;

      case 'es_301':
        this.esignErrorDesc = "Invalid Transaction Id"
        break;

      case 'es_302':
        this.esignErrorDesc = "Invalid Device Name"
        break;
      case 'es_303':
        this.esignErrorDesc = "Invalid Device Serial Number"
        break;
      case 'es_304':
        this.esignErrorDesc = "Duplicate Reference Id"
        break;
      default:
        this.esignErrorDesc = "Unknown Error"

    }
  }



  newDocument() {
    this.askAadhaar = true
    this.askAadhaarOTP = false
    this.eSignResponse = false
    this.fileName = ''
    this.aadhaarOTP = ''
    this.aadhaar = ''
    this.termsandconditions = false
  }

  downloadFile() {
    //  this.esignService.downloadEsignedFile(this.fileName )
  }

  uploadFile(item) {
    var resp;
    resp = item.upload();

    this.uploader.onCompleteItem = (item: any, response: any, status: any, headers: any) => {
      var responsePath = JSON.parse(response);
      this.askAadhaar = true;
      this.uploadedFileName = responsePath.fileInfo.filename

      this.currentStep = 'step2';
    };
  }



  ngOnInit(): void {
    //generate random values for mainChart

    this.route.params.subscribe(params => {
      this.documentId = params['documentId']
      this.signerId = params['signerId']
      this.signerSequence = params['signerSequence']
      this.userId = params['userId']


      this.esignService.getDocumentInfo(this.documentId)
        .subscribe(response => {

          var createdDate = new Date(response.created_at);
          console.log(createdDate)

          this.latest_document = response.latest_document

          this.document_path = response.document_path;

          if (this.document_path == this.latest_document) {
            console.log('FirstSigner')
            this.isFirstSigner = true;
          } else {
            this.isFirstSigner = false;
          }


          this.documentTitle = response.document_title
          this.filename = response.document_name
          this.fileSource = this.appConfig.serverAddress + '/api/esign/viewfile/' + this.documentId;
          (<HTMLInputElement>document.getElementById('file_preview')).src = this.fileSource
          // 8:02 PM 12 FEB 2014
          this.createdDateTime = (createdDate.getMonth() + 1) + "-" + createdDate.getDate() + "-" + createdDate.getFullYear();



          this.esignService.getSignerInfo(this.signerId)
            .subscribe(response => {
              if (response.status === 'signed') {
                this.currentStep = 'step8';
                this.otpError = false;
                this.esignError = false;
                this.esignedFileSource = this.appConfig.serverAddress + '/api/esign/download/' + this.documentId;



              } else if (response.status === 'invited') {
                var param = {
                  documentId: this.documentId,
                  signerId: this.signerId,
                  status: 'viewed'
                }

                this.esignService.updateSignerStatus(param)
                  .subscribe(response => {
                  });
              }
            });




        });


      this.esignService.getSupportingDocuments(this.documentId)
        .subscribe(response => {
          this.documentsList = response;
          console.log(this.documentsList)
        });


    });
  }
}
