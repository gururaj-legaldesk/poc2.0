import { Component, OnInit } from '@angular/core';
import { Router,ActivatedRoute } from '@angular/router';
import {Http, Headers} from '@angular/http';
import 'rxjs/add/operator/map';
import { DocumentService } from '../services/document.service';
import { EsignService } from '../services/esign.service';
import { FileUploader } from 'ng2-file-upload';
import { Config } from '../config';

@Component({
  templateUrl: 'viewdocument.component.html'
})

export class ViewDocumentComponent implements OnInit {


  public appConfig           = new Config();

  public sourceType:string =  '';
  public currentStep:string =  'step1';


  public documentId:string

  public documentTitle:string
  public filename:string
  public fileSource:string
  public createdDateTime:string

  public signerList          = []
  // Set our default values
  localState = { value: '' };
  fileName ='';
  askAadhaar:boolean;
  askAadhaarOTP =false;
  aadhaar:string;
  aadhaarOTP:string;
  aadhaarError :boolean;
  aadhaarOtpError :boolean;
  eSignResponse: boolean;
  termsandconditions :boolean;
  esignedFileSource :string;
  uploadedFileName:string;
  transactionId:string;
  loading:boolean; otpError:boolean;
 documentsList: Array<number> = [];

  public uploader:FileUploader = new FileUploader({url:this.appConfig.serverAddress+'/upload'});


  // TypeScript public modifiers
  constructor( private router: Router ,private http:Http,private esignService:EsignService,private route:ActivatedRoute) {
    this.sourceType  = "";
    this.currentStep = 'step1';

    this.aadhaar=  "";
  }


  fileEvent(fileInput: any){
    let file = fileInput.target.files[0];
    this.fileName = file.name;


    if (fileInput.target.files && fileInput.target.files[0]) {
      var reader = new FileReader();

      reader.onload = function (e : any) {
        (<HTMLInputElement>document.getElementById('iframe')).src= e.target.result;

        // document.getElementById('iframe').src= e.target.result;
        //   console.log('file was selected:' , e.target.result);
        //  $('#preview').attr('src', e.target.result);
      }

      reader.readAsDataURL(fileInput.target.files[0]);


    }
  }

  validateAadhaar(){
    if(!this.aadhaar) {
      this.aadhaarError = true
    }else if(this.aadhaar && this.aadhaar.toString().length<4){
      this.aadhaarError  = true
    }else{
      this.aadhaarError  = false
      this.loading       = true

      this.esignService.getTriggarOTP(this.aadhaar)
        .subscribe(response => {

          var responseJson = JSON.parse(response);
          this.loading       = false
          this.askAadhaar    = false


          console.log('apiResponse error',responseJson.Error)
          if(responseJson.ErrorCode=="NA") {
            this.askAadhaarOTP = true
            this.transactionId  = responseJson.Transaction_Id;
            this.otpError =false
          }else {
            this.askAadhaar    = true
            this.otpError      = true
            this.loading       = false
          }
        });
    }
  }


  validateOTP() {

    if (!this.aadhaarOTP) {
      this.aadhaarOtpError  = true
    } else if (this.aadhaarOTP && this.aadhaarOTP.toString().length < 4) {
      this.aadhaarOtpError  = true
    } else if(!this.termsandconditions){
      this.aadhaarOtpError  = true
    }else{
      this.aadhaarOtpError  = false

      //invoke Esign api here
      this.loading       = true
      var params= {
        aadhaar: this.aadhaar,
        otp :this.aadhaarOTP,
        transactionId: this.transactionId, //"c5292174c75739ba7b8fb9898a0c854c",, //this.transactionId,
        file: this.uploadedFileName,
        referenceId:"signdesk"+Date.now().toString() //this.transactionId
      }

      this.esignService.getSignedDocument(params)
        .subscribe(response => {
          console.log(response)

          this.loading       = false
          this.askAadhaarOTP = false
          this.eSignResponse = true

          if(response.toString().trim()=="Signed") {
            this.otpError          = false
            this.esignedFileSource =this.appConfig.serverAddress+'/api/esign/download/signed_'+this.uploadedFileName;

          }else {
            this.askAadhaarOTP = false
            this.otpError      = true
            this.loading       = false
            this.eSignResponse = true
          }
        });





    }



  }


  newDocument() {
    this.askAadhaar    = true
    this.askAadhaarOTP = false
    this.eSignResponse = false
    this.fileName      = ''
    this.aadhaarOTP    = ''
    this.aadhaar       = ''
    this.termsandconditions   = false
  }

  downloadFile() {
    //  this.esignService.downloadEsignedFile(this.fileName )
  }

  uploadFile(item) {
    var resp;
    resp = item.upload();

    this.uploader.onCompleteItem = (item: any, response: any, status: any, headers: any) => {
      var responsePath = JSON.parse(response);
      this.askAadhaar = true;
      this.uploadedFileName =responsePath.fileInfo.filename

      this.currentStep = 'step2';
    };
  }



  ngOnInit(): void {
    this.route.params.subscribe(params => {
      this.documentId=params['documentId']


      this.esignService.getDocumentInfo(this.documentId)
        .subscribe(response => {


          var createdDate = new Date(response.created_at);
          console.log(createdDate)


          this.documentTitle  = response.document_title
          this.filename       = response.document_name
          this.fileSource     = this.appConfig.serverAddress+'/api/esign/viewfile/'+this.documentId;
          (<HTMLInputElement>document.getElementById('file_preview')).src= this.fileSource
          // 8:02 PM 12 FEB 2014
          this.createdDateTime      = (createdDate.getMonth() + 1) + "-" + createdDate.getDate() + "-" + createdDate.getFullYear();

          console.log(response)
        });



      this.esignService.getDocumentSingerInfo(this.documentId)
        .subscribe(response => {
          this.signerList = response
          console.log(response)
        });





      this.esignService.getSupportingDocuments(this.documentId)
      .subscribe(response =>{
        this.documentsList=response;
      console.log(this.documentsList)
      });





    });
  }
}
