import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { DocumentsComponent } from './documents.component';
import { CreateDocumentComponent } from './createdocument.component';
import { ViewDocumentComponent } from './viewdocument.component';
import { ViewSupportingDocumentComponent } from './viewsupportingdocument.component';
import { SignDocumentComponent } from './signdocument.component';


const routes: Routes = [
  {
    path: '',
    //component: DocumentsComponent,
    data: {
      title: 'documents'
    },
    children: [
      {
        path: '',
        component: DocumentsComponent,
        data: {
          title: 'Font Awesome'
        }
      },
      {
        path: 'create',
        component: CreateDocumentComponent,
        data: {
          title: 'Create New Document'
        }
      },
      {
        path: 'view/:documentId',
        component: ViewDocumentComponent,
        data: {
          title: 'View Document'
        }
      },
       {
        path: 'view/supportingdocuments/:documentId/:supportingDocId',
        component: ViewSupportingDocumentComponent,
        data: {
          title: 'View supporting Document'
        }
      },
      {
        path: 'view/:documentId/:userId/:signerSequence/:signerId',
        component: SignDocumentComponent,
        data: {
          title: 'eSign Document'
        }
      }

    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DocumentsRoutingModule {}
