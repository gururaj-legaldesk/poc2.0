import { NgModule }            from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { DataTableModule } from 'angular2-datatable';
import { HttpModule } from '@angular/http';

import { DataFilterPipe } from './datafilterpipe';


import { ActivityLogComponent }      from './activity_log.component';
import { ActivityLogRoutingModule }  from './activity_log-routing.module';
import { ActivityLogService } from '../services/activity_log.service';


@NgModule({
  imports: [
    ActivityLogRoutingModule,CommonModule,DataTableModule,FormsModule,HttpModule
  ],
  declarations: [ ActivityLogComponent,DataFilterPipe ],
  providers: [ 
    ActivityLogService
  ]
})
export class ActivityLogModule { }
