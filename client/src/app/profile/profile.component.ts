import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import {Http, Headers} from '@angular/http';
import 'rxjs/add/operator/map';
import { ProfileService } from '../services/profile.service';



@Component({
  templateUrl: 'profile.component.html'
})
export class ProfileComponent implements OnInit {

 public data;
 public data2;
  public filterQuery = '';

  constructor(private http:Http) {
    http.get('data.json')
      .subscribe((data)=> {
        setTimeout(()=> {
          this.data = data.json();
            this.data2 = data.json();
        }, 2000);
      });
}

  public toInt(num:string) {
    return +num;
  }

  public sortByWordLength = (a:any) => {
    return a.name.length;
  }  

  ngOnInit(): void {


  }
}


